import pathResolver from '../../helpers/path-resolver';
import enums from '../../helpers/enums';
import { headers } from '../../helpers/headers';

module.exports = async (req, res) => {
  headers.forEach((header) => {
    res.setHeader(header.key, header.value);
  });
  if (req.method !== 'DELETE') {
    res.status(404).send();
    return;
  }
  const response = await pathResolver.call(
    req.url,
    enums.DELETE,
    JSON.parse(req.body)
  );
  res.status(response.status).send(response.data);
};
