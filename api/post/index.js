import pathResolver from '../../helpers/path-resolver';
import enums from '../../helpers/enums';
import { headers } from '../../helpers/headers';

module.exports = async (req, res) => {
  headers.forEach((header) => {
    res.setHeader(header.key, header.value);
  });
  try {
    const response = await pathResolver.call(
      req.url,
      enums.POST,
      JSON.parse(req.body),
      req.headers.authorization
    );
    res.status(response.status).send(response.data);
  } catch (error) {
    res.status(503).send(error.toString());
  }
};
