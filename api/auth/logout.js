const cookie = require('cookie');
const { headers } = require('../../helpers/headers.js');

module.exports = (req, res) => {
  headers.forEach((header) => {
    res.setHeader(header.key, header.value);
  });
  if (req.method === 'OPTIONS') {
    res.status(200).send();
    return;
  }
  try {
    res.setHeader(
      'Set-Cookie',
      cookie.serialize('rid', 'dead', {
        httpOnly: true,
        maxAge: 0,
        secure: true,
        sameSite: 'none',
        path: '/',
      })
    );
    res.status(200).send({});
  } catch (e) {
    console.error(e);
    res.status(500).send();
  }
};
