const jwt = require('jsonwebtoken');
const { headers } = require('../../lib/headers.js');

module.exports = (req, res) => {
  headers.forEach((header) => {
    res.setHeader(header.key, header.value);
  });
  if (req.method === 'OPTIONS') {
    res.status(200).send();
    return;
  }
  try {
    const refreshToken = req.headers.cookie && req.headers.cookie.split('=')[1];
    let accessToken = '';
    try {
      jwt.verify(refreshToken, process.env.SEC_REFRESH);
    } catch (e) {
      console.log(e.message);
      res.status(401).send();
      return;
    }
    accessToken = jwt.sign({ sent: Date.now() }, process.env.SEC_ACCESS, {
      expiresIn: '30m',
    });
    res.status(200).send({ accessToken });
  } catch (e) {
    console.error(e);
    res.status(500).send();
  }
};
