const jwt = require('jsonwebtoken');
const cookie = require('cookie');
const { headers } = require('../../helpers/headers');

module.exports = (req, res) => {
  headers.forEach((header) => {
    res.setHeader(header.key, header.value);
  });
  if (req.method === 'OPTIONS') {
    res.status(200).send();
    return;
  }
  try {
    const { password } = JSON.parse(req.body);
    if (password === process.env.JWT_SECRET) {
      let refreshToken = jwt.sign(
        { sent: Date.now() },
        process.env.SEC_REFRESH,
        {
          expiresIn: '2d',
        }
      );
      let accessToken = jwt.sign({ sent: Date.now() }, process.env.SEC_ACCESS, {
        expiresIn: '30m',
      });
      res.setHeader(
        'Set-Cookie',
        cookie.serialize('rid', refreshToken, {
          httpOnly: true,
          maxAge: 60 * 60 * 24 * 2,
          secure: true,
          path: '/',
          sameSite: 'none',
        })
      );
      res.status(200).send({ accessToken });
    } else {
      res.status(401).send();
    }
  } catch (e) {
    console.error(e);
    res.status(500).send();
  }
};
