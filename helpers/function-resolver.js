import getFunctions from "./functions/get-functions";
import postFunctions from './functions/post-functions';
import putFunctions from './functions/put-functions';
import deleteFunctions from './functions/delete-functions';

export default {
  get: getFunctions,
  post: postFunctions,
  update: putFunctions,
  delete: deleteFunctions,
};
