import dbDriver from '../../db-driver';

async function updatePaper({ paper }) {
  //dont forget to sanitize the data/urlencode
  //add all other fields to their respective collections
  //typeofsample, geolitholoy...
  await dbDriver.updateDataInDatabase('papers', paper);
  await dbDriver._closeClient();
}

export default [updatePaper];
