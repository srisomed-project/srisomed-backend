import { ObjectId } from 'mongodb';
import dbDriver from '../../db-driver';
import { verifyToken } from '../../verifyToken';

async function addPapers({ papers }) {
  const mappedPapers = papers.map(async (paper) => {
    let countryId = '';
    let geolithologyId = '';
    let sampleCodeId = '';

    //check if the below returns the corresponding ids

    const country = await dbDriver
      .getDataFromCollection('country', {
        country: paper.country,
      })
      .toArray()[0];

    const geolithology = await dbDriver
      .getDataFromCollection('geolithology', {
        geolithology: paper.geolithology,
      })
      .toArray()[0];

    const sampleCode = await dbDriver
      .getDataFromCollection('sampleCode', {
        sampleCode: paper.sampleCode,
      })
      .toArray()[0];

    if (!country)
      countryid = await dbDriver.insertDataToDatabase('country', {
        countryArea: country,
      });
    else countryId = country._id;

    if (!geolithology)
      geolithologyId = await dbDriver.insertDataToDatabase('geolithology', {
        geolithology: geolithology,
      });
    else geolithologyId = geolithology._id;

    if (!dateOfSample)
      dateOfSampleId = await dbDriver.insertDataToDatabase('date-of-sample', {
        dateOfSample: dateOfSample,
      });
    else dateOfSampleId = dateOfSample._id;

    if (!site)
      siteId = await dbDriver.insertDataToDatabase('site', { site: site });
    else siteId = site._id;

    if (!sampleCode)
      sampleCodeId = await dbDriver.insertDataToDatabase('sampleCode', {
        sampleCode: sampleCode,
      });
    else sampleCodeId = sampleCode._id;

    return {
      ...paper,
      countryArea: countryId,
      geolithology: geolithologyId,
      sampleCode: sampleCodeId,
    };
  });

  await dbDriver.insertDataToDatabase('papers', mappedPapers);
  await dbDriver._closeClient();
}

async function getAllFilteredData({
  selectedResultsPerPage,
  page,
  selectedSampleCodes,
  selectedCountries,
  selectedGeolithology,
  minRatio,
  maxRatio,
}) {
  let limit = Number(selectedResultsPerPage[0].name);
  let offset = Number(page);

  const query = {};
  selectedSampleCodes = selectedSampleCodes.map((elm) => ObjectId(elm.id));
  selectedCountries = selectedCountries.map((elm) => ObjectId(elm.id));
  selectedGeolithology = selectedGeolithology.map((elm) => ObjectId(elm._id));

  query.sampleCode = { $in: selectedSampleCodes };
  if (!selectedSampleCodes.length) delete query.sampleCode;

  query.geolithology = { $in: selectedGeolithology };
  if (!selectedGeolithology.length) delete query.geolithology;

  query.countryArea = { $in: selectedCountries };
  if (!selectedCountries.length) delete query.countryArea;

  const agg = [
    {
      $match: query,
    },
    {
      $match: { ratio: { $gt: minRatio } },
    },
    {
      $match: { ratio: { $lt: maxRatio } },
    },
    {
      $lookup: {
        from: 'country',
        localField: 'countryArea',
        foreignField: '_id',
        as: 'countryArea',
      },
    },
    { $unwind: '$countryArea' },
    {
      $lookup: {
        from: 'geolithology',
        localField: 'geolithology',
        foreignField: '_id',
        as: 'geolithology',
      },
    },
    {
      $unwind: '$geolithology',
    },
    {
      $lookup: {
        from: 'sampleCode',
        localField: 'sampleCode',
        foreignField: '_id',
        as: 'sampleCode',
      },
    },
    { $unwind: '$sampleCode' },
    {
      $facet: {
        metadata: [{ $count: 'total' }],
        papers: [{ $skip: (offset - 1) * limit }, { $limit: limit }],
      },
    },
  ];

  const [papers] = await dbDriver.getDataFromAggregate('papers', agg);
  await dbDriver._closeClient();
  return { data: papers };
}

async function getAllDataWithCoordinates({
  selectedSampleCodes,
  selectedCountries,
  selectedGeolithology,
  minRatio,
  maxRatio,
  limit,
}) {
  limit = Number(limit[0].name);
  const query = {};
  selectedSampleCodes = selectedSampleCodes.map((elm) => ObjectId(elm.id));
  selectedCountries = selectedCountries.map((elm) => ObjectId(elm.id));
  selectedGeolithology = selectedGeolithology.map((elm) => ObjectId(elm._id));

  query.sampleCode = { $in: selectedSampleCodes };
  if (!selectedSampleCodes.length) delete query.sampleCode;

  query.geolithology = { $in: selectedGeolithology };
  if (!selectedGeolithology.length) delete query.geolithology;

  query.countryArea = { $in: selectedCountries };
  if (!selectedCountries.length) delete query.countryArea;

  query.$and = [{ ratio: { $gt: minRatio } }, { ratio: { $lt: maxRatio } }];
  const papersWithCoordinates = await dbDriver.getDataFromCollection(
    'papers',
    {
      longitude: { $ne: '' },
      latitude: { $ne: '' },
      ...query,
    },
    {},
    limit,
    0,
    {
      projection: {
        _id: true,
        longitude: true,
        latitude: true,
        accuracy: true,
      },
    }
  );
  await dbDriver._closeClient();
  return papersWithCoordinates;
}

async function addSubmission({ submission }) {
  const { data, type } = submission;
  switch (type) {
    case 'submission':
      await dbDriver.insertDataToDatabase('submissions', JSON.parse(data));
      break;
    case 'feedback':
      await dbDriver.insertDataToDatabase('feedback', [
        { feedback: data, postedOn: Date.now() },
      ]);
      break;
    default:
      break;
  }
  await dbDriver._closeClient();
}

async function addAllContributed(_) {
  let contributed = (await dbDriver.getDataFromCollection('submissions'))
    .map((elm) => ({ ...elm, contributed: true }))
    .map((elm) => {
      const { _id, ...e } = elm;
      return { ...e };
    });
  const inserted = await dbDriver.insertDataToDatabase('papers', contributed);
  const deleted = await dbDriver.deleteDataFromDatabase('submissions', {
    _id: {
      $in: Object.values(inserted).map((elm) => ObjectId(elm)),
    },
  });
  return [deleted.deletedCount];
}

async function addSomeContibuted({ dataToAdd, dataToRemove }) {
  let inserted = [];
  let contributedSome = [];
  let contributedRemove = [];
  if (dataToAdd.length > 0) {
    contributedSome = (
      await dbDriver.getDataFromCollection('submissions', {
        _id: { $in: dataToAdd.map((elm) => ObjectId(elm)) },
      })
    )
      .map((elm) => ({ ...elm, contributed: true }))
      .map((elm) => {
        const { _id, ...e } = elm;
        contributedRemove.push(_id);
        return { ...e };
      });
    inserted = await dbDriver.insertDataToDatabase('papers', contributedSome);
  }
  const deleted = await dbDriver.deleteDataFromDatabase('submissions', {
    _id: {
      $in: [
        ...Object.values(contributedRemove).map((elm) => ObjectId(elm)),
        ...dataToRemove.map((elm) => ObjectId(elm)),
      ],
    },
  });
  return [deleted.deletedCount];
}

export default [
  addPapers,
  addSubmission,
  getAllFilteredData,
  getAllDataWithCoordinates,
  addAllContributed,
  addSomeContibuted,
];
