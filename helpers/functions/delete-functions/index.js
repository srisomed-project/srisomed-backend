import dbDriver from '../../db-driver';

async function deletePaper(paper) {
  //Delete all other fields from collection if the last field is in collection.
  //typeofsample, geolitholoy...
  await dbDriver.deleteDataFromDatabase('papers', paper);
  await dbDriver._closeClient();
}

async function deleteSubmission(submission) {
  //dont forget to sanitize the data/urlencode
  await dbDriver.deleteDataFromDatabase('submissions', submission);
  await dbDriver._closeClient();
}

export default [deletePaper, deleteSubmission];
