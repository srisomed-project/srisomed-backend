import { ObjectId } from 'mongodb';
import dbDriver from '../../db-driver';

async function getAllData({ limit = 10, offset = 0 }) {
  limit = Number(limit);
  offset = Number(offset);

  const agg = [
    {
      $lookup: {
        from: 'country',
        localField: 'countryArea',
        foreignField: '_id',
        as: 'countryArea',
      },
    },
    { $unwind: '$countryArea' },
    {
      $lookup: {
        from: 'geolithology',
        localField: 'geolithology',
        foreignField: '_id',
        as: 'geolithology',
      },
    },
    {
      $unwind: '$geolithology',
    },
    {
      $lookup: {
        from: 'sampleCode',
        localField: 'sampleCode',
        foreignField: '_id',
        as: 'sampleCode',
      },
    },
    { $unwind: '$sampleCode' },
    { $limit: limit },
    { $skip: offset },
  ];

  const papers = await dbDriver.getDataFromAggregate('papers', agg);
  await dbDriver._closeClient();
  return { data: papers };
}

async function getAllOptions() {
  const countries = await dbDriver.getDataFromCollection('country');
  const sites = await dbDriver.getDataFromCollection('site');
  const geolithology = await dbDriver.getDataFromCollection('geolithology');
  const sampleCodes = await dbDriver.getDataFromCollection('sampleCode');
  const datesOfSamples = await dbDriver.getDataFromCollection('date-of-sample');
  await dbDriver._closeClient();
  return { countries, sites, geolithology, sampleCodes, datesOfSamples };
}

async function getAllCountries() {
  const countries = await dbDriver.getDataFromCollection('country');
  await dbDriver._closeClient();
  return countries;
}

async function getAllSites() {
  const sites = await dbDriver.getDataFromCollection('site');
  await dbDriver._closeClient();
  return sites;
}

async function getAllGeolithology() {
  const geolithology = await dbDriver.getDataFromCollection('geolithology');
  await dbDriver._closeClient();
  return geolithology;
}

async function getAllSampleCodes() {
  const sampleCodes = await dbDriver.getDataFromCollection('sampleCode');
  await dbDriver._closeClient();
  return sampleCodes;
}

async function getAllDatesOfSamples() {
  const datesOfSamples = await dbDriver.getDataFromCollection('date-of-sample');
  await dbDriver._closeClient();
  return datesOfSamples;
}

async function getMinMaxRatio() {
  const min = await dbDriver.getDataFromCollection(
    'papers',
    {},
    { ratio: -1 },
    1,
    0
  );
  const max = await dbDriver.getDataFromCollection(
    'papers',
    {},
    { ratio: 1 },
    1,
    1
  );
  await dbDriver._closeClient();
  return { min: min[0].ratio, max: max[0].ratio };
}

async function getAllDataWithCoordinates({ limit }) {
  const papersWithCoordinates = await dbDriver.getDataFromCollection(
    'papers',
    {
      longitude: { $ne: '' },
      latitude: { $ne: '' },
    },
    {},
    Number(limit),
    0,
    {
      projection: {
        _id: true,
        longitude: true,
        latitude: true,
        accuracy: true,
      },
    }
  );
  await dbDriver._closeClient();
  return papersWithCoordinates;
}

async function getPaper({ id }) {
  const agg = [
    {
      $lookup: {
        from: 'country',
        localField: 'countryArea',
        foreignField: '_id',
        as: 'countryArea',
      },
    },
    { $unwind: '$countryArea' },
    {
      $lookup: {
        from: 'geolithology',
        localField: 'geolithology',
        foreignField: '_id',
        as: 'geolithology',
      },
    },
    {
      $unwind: '$geolithology',
    },
    {
      $lookup: {
        from: 'sampleCode',
        localField: 'sampleCode',
        foreignField: '_id',
        as: 'sampleCode',
      },
    },
    { $unwind: '$sampleCode' },
    { $match: { _id: ObjectId(id) } },
  ];

  const papers = await dbDriver.getDataFromAggregate('papers', agg);
  await dbDriver._closeClient();
  return { data: papers };
}

async function getFeedbackAndSubmissions({ page }) {
  const submissions = await dbDriver.getDataFromCollection(
    'submissions',
    {},
    {},
    1000,
    page
  );
  const submissionsCount = await dbDriver.getCountFromCollection('submissions');
  const feedback = await dbDriver.getDataFromCollection(
    'feedback',
    {},
    {},
    1000,
    page
  );
  await dbDriver._closeClient();
  return { submissions, feedback, submissionsCount };
}

export default [
  getAllData,
  getAllCountries,
  getAllSites,
  getAllGeolithology,
  getAllSampleCodes,
  getAllDatesOfSamples,
  getMinMaxRatio,
  getAllOptions,
  getAllDataWithCoordinates,
  getPaper,
  getFeedbackAndSubmissions,
];
