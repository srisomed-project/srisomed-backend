import { MongoClient } from 'mongodb';

class DBConnection {
  constructor() {
    this.client = new MongoClient(process.env.MONGO_URL, {
      useUnifiedTopology: true,
    });
  }

  async getDataFromAggregate(collection, aggregate) {
    const db = await this._getDB();
    const coll = db.collection(collection);
    const data = await coll.aggregate(aggregate).toArray();
    return data;
  }

  async getDataFromCollection(
    collection,
    queryParam = {},
    sort = {},
    limit = 0,
    offset = 0,
    queryOptions = {}
  ) {
    const db = await this._getDB();
    const coll = await db.collection(collection);
    const data = await coll
      .find(queryParam, queryOptions)
      .sort(sort)
      .limit(limit)
      .skip(offset)
      .toArray();
    return data;
  }

  async getCountFromCollection(collection) {
    const db = await this._getDB();
    const coll = await db.collection(collection);
    const count = await coll.countDocuments();
    return count;
  }

  async insertDataToDatabase(collection, data) {
    const db = await this._getDB();
    const coll = await db.collection(collection);
    const _id = await coll.insertMany(data);
    return _id.insertedIds;
  }

  async deleteDataFromDatabase(collection, data) {
    const db = await this._getDB();
    const coll = await db.collection(collection);
    const details = await coll.deleteMany(data);
    return details;
  }

  async updateDataInDatabase(collection, data) {
    const coll = await db.collection(collection);
    await coll.updateOne(data);
  }

  async _getDB(db = '') {
    return (await this.client.connect()).db(db);
  }

  async _closeClient() {
    await (await this.client).close();
  }
}

export default new DBConnection();
