import METHODS from './enums';
import functionResolver from './function-resolver';
import { verifyToken } from './verifyToken';
const resolve = (path) => {
  return path
    .split('?')[1]
    .split('&')
    .map((params) => params.split('='))
    .map(([key, value]) => ({ [key]: value }))
    .reduce((prev, curr) => {
      prev = { ...curr, ...prev };
      return prev;
    }, {});
};

export default {
  call: async (uri, method, body, auth) => {
    const protectedFunctions = [['10'], ['4', '5']];
    if (
      auth &&
      (protectedFunctions[method].includes(resolve(uri).func) ||
        protectedFunctions[method].includes(body.func))
    ) {
      const token = auth.split(' ')[1];
      if (!verifyToken(token)) {
        return { status: 401, data: 'Unauthorized!' };
      }
    }

    let response = [];

    switch (method) {
      case METHODS.GET:
        const url = resolve(uri);
        response = await functionResolver.get[url.func]({
          limit: url.limit,
          offset: url.offset,
          id: url.id,
        });
        break;
      case METHODS.POST:
        response = await functionResolver.post[body.func](body.query);
        break;
      // unused
      case METHODS.PUT:
        response = await functionResolver.put[body.func](body);
        break;
      // unused
      case METHODS.DELETE:
        response = await functionResolver.delete[body.func](body);
        break;
      default:
        response = { status: 500, data: new Error('Unsupported method') };
        break;
    }
    return { status: 200, data: response };
  },
};
