const jwt = require('jsonwebtoken');

export const verifyToken = (token) => {
  try {
    jwt.verify(token, process.env.SEC_ACCESS);
    return true;
  } catch (e) {
    return false;
  }
};
